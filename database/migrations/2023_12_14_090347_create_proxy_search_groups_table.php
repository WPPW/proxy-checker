<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create('proxy_search_groups', function (Blueprint $table) {
            $table->id();
            $table->integer('total_proxies')->default(0); // Общее количество проверенных прокси
            $table->integer('alive_proxies')->default(0); // Количество живых прокси
            $table->float('total_duration')->default(0.0); // Общее время проверки в секундах
            $table->timestamps(); // Для сохранения времени создания и обновления группы
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('proxy_search_groups');
    }
};

