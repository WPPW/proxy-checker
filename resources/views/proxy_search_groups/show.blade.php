<div>
    <h3>Детали проверки от {{ $searchGroup->created_at }}</h3>
    Детали проверки

    @foreach ($searchGroup->proxyCheckResults as $result)
        <p>{{ $result->proxy }} - {{ $result->is_worked_status ? 'Работает' : 'Не работает' }}</p>
    @endforeach
</div>
