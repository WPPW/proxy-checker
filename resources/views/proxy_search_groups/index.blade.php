@foreach ($searchGroups as $group)
    <div>
        <a href="{{ route('proxy-search-groups.show', $group) }}">
            Проверка от {{ $group->created_at }}
            ({{ $group->total_proxies }} прокси,
            {{ $group->alive_proxies }} живых,
            заняло {{ $group->total_duration }} сек.)
        </a>
    </div>
@endforeach
