{{-- resources/views/proxy_checker.blade.php --}}
        <!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/5.2/css/bootstrap.min.css">
    <title>Proxy Checker</title>
    <style>
        /* Здесь можно добавить CSS-стили */
        textarea {
            width: 100%;
            padding: 10px;
            margin: 10px 0;
            border: 1px solid #ddd;
            border-radius: 4px;
        }

        button {
            padding: 10px 20px;
            background-color: blue;
            color: white;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }
    </style>
</head>
<body class="antialiased">
<div class="container">
    <h1>Proxy Checker</h1>
    <form id="proxyCheckerForm">
        @csrf <!-- Laravel CSRF token для безопасности -->
        <textarea name="proxies" rows="10" cols="50"
                  placeholder="Введите прокси в формате ip:port, по одному на строку"></textarea>
        <br>
        <button type="submit">Проверить прокси</button>
    </form>
    <br>

    <h2>Результаты проверки (данные обновляются раз в 5 секунд)</h2>
    <a href="{{ route('proxy-search-groups.index') }}">Посмотреть все результаты</a>
    <ul id="results"></ul>
</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
    // Обработчик отправки формы
    $('#proxyCheckerForm').submit(function () {
        // Отправка данных на сервер
        $.ajax({
            url: '/proxy-check',
            method: 'POST',
            data: $(this).serialize(),
            success: function (data) {
                // Вывод сообщения об успешной отправке
                alert(data.msg);
            }
        });
        return false; // Отмена отправки формы
    });

    // Переменная для хранения времени последнего полученного результата
    let lastId = 0;

    function fetchResults() {
        $.ajax({
            url: '/proxy-check-results',
            data: {lastId: lastId},
            success: function (data) {
                if (data.length > 0) {
                    console.log(data)
                    // Обновление времени последнего полученного результата
                    lastId = data[data.length - 1].id;
                }


                // Добавление новых результатов
                data.forEach(function (result) {
                    var resultText = 'Прокси: ' + result.proxy +
                        ', Работает: ' + (result.is_worked_status ? 'Да' : 'Нет') +
                        ', Тип: ' + result.type +
                        ', Страна: ' + result.country +
                        ', Город: ' + result.city +
                        ', Скорость: ' + result.speed +
                        ', Внешний IP: ' + result.external_ip;
                    $('#results').append('<li>' + resultText + '</li>');
                });

            }
        });

        // Повторный опрос через некоторое время
        setTimeout(fetchResults, 5000); // Повторный опрос каждые 5 секунд

    }

    // Начальный вызов функции
    fetchResults();
</script>

</body>
</html>
