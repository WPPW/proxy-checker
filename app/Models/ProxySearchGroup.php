<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProxySearchGroup extends Model
{
    protected $fillable = [
        'total_proxies',
        'alive_proxies',
        'total_duration',
    ];

    // Отношения
    public function proxyCheckResults()
    {
        return $this->hasMany(ProxyCheckResult::class);
    }
}
