<?php

namespace App\Domain\Proxy\Actions;

use Illuminate\Support\Facades\Http;

class GetProxyGeoData
{
    public function __construct()
    {
    }

    public function do(string $proxy): array
    {
        // Получаем IP-адрес прокси
        $proxyIP = explode(':', $proxy)[0];
        // Получение геоданных
        $geoData = Http::get('http://ip-api.com/json/' . $proxyIP);
        $country = $geoData['country'] ?? '';
        $city    = $geoData['city'] ?? '';

        return [
            'country' => $country,
            'city'    => $city,
        ];
    }
}