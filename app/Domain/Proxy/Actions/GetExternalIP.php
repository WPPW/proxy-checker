<?php

namespace App\Domain\Proxy\Actions;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class GetExternalIP
{
    public function __construct()
    {
    }

    public function do($proxy)
    {
        $client = new Client();

        try {
            $response = $client->request('GET', 'https://httpbin.org/ip', [
                'proxy'   => $proxy,
                'timeout' => 10,
            ]);

            $data = json_decode($response->getBody(), true);

            return $data['origin'] ?? null;
        } catch (GuzzleException $e) {
            // Обработка ошибок соединения
            return null;
        }
    }
}