<?php

namespace App\Domain\Proxy\Actions;

use App\Models\ProxyCheckResult;
use App\Models\ProxySearchGroup;

readonly class CheckOneProxy
{
    public function __construct(
        private GetOneProxyDetails $getOneProxyDetails,
    ) {
    }

    public function do(string $proxy, int $groupId): void
    {
        // Тестовый URL, на который будет идти запрос
        $testUrl = 'https://httpbin.org/ip';

        // Типы прокси
        $proxyTypes = [
            CURLPROXY_SOCKS5 => 'SOCKS5',
            CURLPROXY_SOCKS4 => 'SOCKS4',
            CURLPROXY_HTTPS  => 'HTTPS',
            CURLPROXY_HTTP   => 'HTTP',
        ];

        // Проверка типа прокси
        foreach ($proxyTypes as $proxyType => $typeName) {
            $result = $this->getOneProxyDetails->do($testUrl, $proxy, $proxyType, $typeName, $groupId);

            if ($result) {
                // Выход из цикла после успешного соединения с прокси
                break;
            }
            // В случае ошибки, цикл продолжится со следующим типом прокси
        }

        // Все попытки соединения неудачны
        if ( ! $result) {
            // Обработка неудачного случая
            ProxyCheckResult::create([
                'proxy'                 => $proxy,
                'is_worked_status'      => false,
                'type'                  => 'unknown',
                'country'               => 'unknown',
                'city'                  => 'unknown',
                'speed'                 => 'unknown',
                'external_ip'           => 'unknown',
                'proxy_search_group_id' => $groupId,
            ]);
        }
    }
}