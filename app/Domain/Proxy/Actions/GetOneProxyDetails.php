<?php

namespace App\Domain\Proxy\Actions;

use App\Models\ProxyCheckResult;
use Illuminate\Support\Facades\Log;

readonly class GetOneProxyDetails
{
    public function __construct(
        private GetProxyGeoData $getProxyGeoData,
        private GetExternalIP $getExternalIP,
    ) {
    }

    public function do(
        string $testUrl,
        string $proxy,
        int|string $proxyType,
        string $typeName,
        int $groupId
    ): string|bool {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $testUrl);
        curl_setopt($ch, CURLOPT_PROXY, $proxy);
        curl_setopt($ch, CURLOPT_PROXYTYPE, $proxyType);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $startTime  = microtime(true);
        $result     = curl_exec($ch);
        $proxySpeed = microtime(true) - $startTime;
        $curlError  = curl_error($ch);
        Log::error($curlError);

        curl_close($ch);

        // Успешное соединение
        if ($result) {
            // Получение геоданных (страна, город)
            [$country, $city] = $this->getProxyGeoData->do($proxy);

            // Получение внешнего IP
            $externalIP = $this->getExternalIP->do($proxy);

            // Сохраняем результаты в базу данных
            ProxyCheckResult::create([
                'proxy'                 => $proxy,
                'is_worked_status'      => true,
                'type'                  => $typeName,
                'country'               => $country,
                'city'                  => $city,
                'speed'                 => $proxySpeed,
                'external_ip'           => $externalIP,
                'proxy_search_group_id' => $groupId,
            ]);
        }

        return $result;
    }
}