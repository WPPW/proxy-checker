<?php

namespace App\Http\Controllers;

use App\Domain\Proxy\Actions\CheckOneProxy;
use App\Domain\Proxy\Actions\ProxyValidateFormat;
use App\Jobs\CheckProxyJob;
use App\Models\ProxySearchGroup;
use Illuminate\Http\JsonResponse;

class ProxyCheckController extends Controller
{
    public function __construct(
        private readonly ProxyValidateFormat $proxyValidateFormat,
    ) {
    }

    public function __invoke(): JsonResponse
    {
        // Получение данных из формы
        $proxies = request('proxies');
        if ( ! $proxies) {
            return response()->json([
                'msg' => 'Вы не указали прокси',
            ], 400);
        }

        // Валидация формата прокси
        $validProxiesArray = $this->proxyValidateFormat->do($proxies);
        if ( ! $validProxiesArray) {
            return response()->json([
                'msg' => 'Похоже, вы не указали ни одного прокси в верном формате',
            ], 400);
        }

        // Создание новой группы
        $searchGroup = ProxySearchGroup::create();

        $totalProxies = count($validProxiesArray);
        $startTime    = microtime(true);

        // Отправка прокси на проверку в очередь
        foreach ($validProxiesArray as $proxy) {
            // Запуск задачи в очереди. Многопоточность можно достичь, запустив несколько воркеров
            CheckProxyJob::dispatch($proxy, $searchGroup->id);
            // Или можно запустить проверку прокси синхронно
            //app(CheckOneProxy::class)->do($proxy, $searchGroup->id);
        }

        // Время выполнения проверки
        $totalDuration = microtime(true) - $startTime;

        // Обновление группы с количеством прокси и временем проверки
        $searchGroup->update([
            'total_proxies'  => $totalProxies,
            'total_duration' => $totalDuration,
        ]);


        return response()->json([
            'msg' => 'Похоже, всё в порядке. Проверка прокси запущена',
        ], 200);
    }
}