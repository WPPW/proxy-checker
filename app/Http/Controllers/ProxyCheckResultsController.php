<?php

namespace App\Http\Controllers;

use App\Domain\Proxy\Actions\ProxyValidateFormat;
use App\Jobs\CheckProxyJob;
use App\Models\ProxyCheckResult;
use Illuminate\Http\JsonResponse;

class ProxyCheckResultsController extends Controller
{
    public function __construct()
    {
    }

    public function __invoke(): JsonResponse
    {
        $lastId = request('lastId', 0);

        // Получение результатов проверки прокси
        $results = ProxyCheckResult::where('id', '>', $lastId)
                                   ->orderBy('id', 'asc')
                                   ->limit(10)
                                   ->get();

        return response()->json($results);
    }
}