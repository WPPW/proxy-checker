<?php

namespace App\Http\Controllers;

use App\Models\ProxySearchGroup;

class ProxySearchGroupController extends Controller
{
    public function index()
    {
        $searchGroups = ProxySearchGroup::with('proxyCheckResults')
                                        ->orderBy('created_at', 'desc')
                                        ->get();

        return view('proxy_search_groups.index', compact('searchGroups'));
    }

    public function show(ProxySearchGroup $searchGroup)
    {
        return view('proxy_search_groups.show', compact('searchGroup'));
    }
}