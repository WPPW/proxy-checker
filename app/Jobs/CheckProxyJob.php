<?php

namespace App\Jobs;

use App\Domain\Proxy\Actions\CheckOneProxy;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CheckProxyJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct(
        protected string $proxy,
        protected int $groupId,
    ) {
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        // Каждый прокси мы проверяем тут
        app(CheckOneProxy::class)->do($this->proxy,$this->groupId);
    }
}
