<?php

use App\Http\Controllers\ProxyCheckController;
use App\Http\Controllers\ProxyCheckResultsController;
use App\Http\Controllers\ProxySearchGroupController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('proxy_checker');
});

// Отправляем прокси на проверку
Route::post('/proxy-check', ProxyCheckController::class);

// Получение результатов проверки прокси (для AJAX)
Route::get('/proxy-check-results', ProxyCheckResultsController::class);

// Страницы с результатами проверки прокси
Route::get('/proxy-search-groups', [ProxySearchGroupController::class, 'index'])->name('proxy-search-groups.index');
Route::get('/proxy-search-groups/{searchGroup}', [ProxySearchGroupController::class, 'show'])->name('proxy-search-groups.show');